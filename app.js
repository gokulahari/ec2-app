const express = require("express");
const app = express();
const path = require("path");
var engines = require("consolidate");
const port = 3000;

app.set("views", __dirname + "/views");
app.engine("html", engines.mustache);
app.set("view engine", "html");

app.get("/", (req, res) => {
  res.render("index");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
